FROM python:2.7-alpine
ENV PYTHONUNBUFFERED 1

RUN apk update && apk add py-psycopg2 unzip

#### APP ######
ADD . /code
WORKDIR /code

RUN pip install --upgrade pip
RUN pip install -Ue .
RUN src/memopol/bin/install_client_deps.sh

ENV DJANGO_SETTINGS_MODULE=memopol.settings
